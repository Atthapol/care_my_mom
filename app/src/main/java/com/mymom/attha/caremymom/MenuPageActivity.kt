package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu_page.*

class MenuPageActivity : AppCompatActivity() {
    override fun onBackPressed() {
        moveTaskToBack(true)
        android.os.Process.killProcess(android.os.Process.myPid())
        System.exit(1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_page)

        btn_notification.setOnClickListener {
            val intent = Intent(this, CalendarActivity::class.java)
            startActivity(intent)
        }

        btn_food.setOnClickListener {
            val intent = Intent(this, MenuFoodActivity::class.java)
            startActivity(intent)
        }

        btn_excersice.setOnClickListener {
            val intent = Intent(this, MenuPageExercise_Activity::class.java)
            startActivity(intent)
        }

        btn_music.setOnClickListener{
            val intent = Intent(this,MenuPageMusic_Activity::class.java)
            startActivity(intent)
        }

        btn_account.setOnClickListener{
            val intent = Intent(this,Page_User_Activity::class.java)
            startActivity(intent)
        }
    }
}
