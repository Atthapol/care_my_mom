package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu__sub__page_exercise2.*

class Menu_Sub_PageExercise2_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu__sub__page_exercise2)
        btn_ex2_1_mn.setOnClickListener{
            val intent = Intent(this,PageExercise2_1_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex2_2_mn.setOnClickListener{
            val intent = Intent(this,PageExercise2_2_Activity::class.java)
            startActivity(intent)
        }
        btn_ex2_3_mn.setOnClickListener{
            val intent = Intent(this,PageExercise2_3_Activity::class.java)
            startActivity(intent)
        }
        btn_ex2_4_mn.setOnClickListener{
            val intent = Intent(this,PageExercise2_4_Activity::class.java)
            startActivity(intent)
        }
        btn_back_menu_sub_page2.setOnClickListener {
            val Intent = Intent(this,MenuPageExercise_Activity::class.java)
            startActivity(Intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,MenuPageExercise_Activity::class.java)
        startActivity(intent)
    }
}
