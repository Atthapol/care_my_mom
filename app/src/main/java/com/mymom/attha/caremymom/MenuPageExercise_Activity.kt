package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu_page_exercise.*

class MenuPageExercise_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_page_exercise)
        btn_menu_ex1.setOnClickListener{
            val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(intent)
        }
        btn_menu_ex2.setOnClickListener{
            val intent = Intent(this,Menu_Sub_PageExercise2_Activity::class.java)
            startActivity(intent)
        }
        btn_back_menu_page.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val Intent = Intent(this,MenuPageActivity::class.java)
        startActivity(Intent)
    }
}
