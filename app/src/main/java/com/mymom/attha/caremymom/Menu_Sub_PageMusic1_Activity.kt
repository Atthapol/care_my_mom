package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu__sub__page_music1.*

class Menu_Sub_PageMusic1_Activity : AppCompatActivity() {

    override fun onBackPressed() {
        val Intent = Intent(this,MenuPageMusic_Activity::class.java)
        startActivity(Intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu__sub__page_music1)

        btn_back_menu_sub_music1.setOnClickListener {
            val intent = Intent(this, MenuPageMusic_Activity::class.java)
            startActivity(intent)
        }
        btn_music1_mn.setOnClickListener {
            val music_header:String = "วัดเอ๋ยวัดโบสถ์"
            val music_name:String = "wat_both_aoei"
            val intent = Intent(this, PageMusic1_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music2_mn.setOnClickListener {
            val music_header:String = "จันทร์เอ๋ยจันทร์เจ้า"
            val music_name:String = "jan_jao"
            val intent = Intent(this, PageMusic1_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music3_mn.setOnClickListener {
            val music_header:String = "จับปูดำขยำปูนา"
            val music_name:String = "jab_pu_dum_ka_yum_pu_na"
            val intent = Intent(this, PageMusic1_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music4_mn.setOnClickListener {
            val music_header:String = "โยกเยก"
            val music_name:String = "yok_yek"
            val intent = Intent(this, PageMusic1_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music5_mn.setOnClickListener {
            val music_header:String = "แมงมุมลายตัวนั้น"
            val music_name:String = "mang_mum"
            val intent = Intent(this, PageMusic1_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }
}
