package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_13.*

class PageExercise1_13_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_13)
        var textSring = getString(R.string.tab)+"นั่งเหยียดเท้าทั้งสองตรงไปข้างหน้า หายใจเข้ายกแขนทั้งสองขึ้น เหนือศีรษะ ต่อไปหายใจออก ค่อย ๆ ก้มตัวลงเท่าที่ทำได้ ไม่ให้กดท้อง ยืดกล้ามเนื้อ " +
                "แผ่นหลังเต็มที่ มือทั้งสองจับข้อเท้าหรือหัวแม่เท้าไว้ค้างไว้ 10 วินาที ทำซ้ำ 2 - 3 ครั้ง<br><br>"+getString(R.string.tab)+"<b>ประโยชน์</b> ท่านี้ช่วยแก้อาการปวดหลังปวดเอว ช่วยให้การย่อยอาหารดี รักษาโรคของ อุ้งเชิงกราน มดลูกและรังไข่"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
