package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_calendar.*
import java.util.Calendar;

class CalendarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        btn_back.setOnClickListener{
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }

        btn_addEvent.setOnClickListener{
            val calendarEvent = Calendar.getInstance()
            val i = Intent(Intent.ACTION_EDIT)
            i.type = "vnd.android.cursor.item/event"
            i.putExtra("beginTime", calendarEvent.getTimeInMillis())
            i.putExtra("allDay", true)
            i.putExtra("rule", "FREQ=YEARLY")
            i.putExtra("endTime", calendarEvent.getTimeInMillis() + 60 * 60 * 1000)
            i.putExtra("title", "เรื่องการแจ้งเตือน")
            startActivity(i)
        }

        btn_menu.setOnClickListener{
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,MenuPageActivity::class.java)
        startActivity(intent)
    }
}
