package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_6.*

class PageExercise1_6_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_6)
        var textSring = getString(R.string.tab)+"1) นอนราบกับพื้นเท้าชิดแขนทั้งสองวางข้างลำตัว หายใจเข้ายกแขนทั้งสองขึ้นแล้ววางแขนทั้งสองลงเหนือศีรษะวางราบกับพื้น แล้วเหยียดแขนทั้งสองออกเต็มที่ทั้งสองเหยียดออกกดปลายเท้าจิกพื้นค้างไว้สักครู่<br><br> "+getString(R.string.tab)+"2) หายใจออกยกแขนทั้งสองขึ้นเหนือศีรษะค่อย ๆ" +
                " วางแขนกลับลงมาข้างลำตัวกลับสู่ท่าเดิมทำซ้ำเช่นนี้ 3 - 5 ครั้ง<br><br>"+getString(R.string.tab)+" <b>ประโยชน์</b> ท่านี้ช่วยให้กล้ามเนื้อแขน ขา ไหล่ มีความตึงตัวแต่พอดีลดความเครียดของกล้ามเนื้อ ทำให้ทรวงอกขยาย และช่วยให้การไหลเวียนโลหิตดีขึ้นเกร็งขา หลังจากที่ฝึกท่าเหยียดตัวแล้วให้เกร็งที่ขาและน่องเพื่อกระตุ้นการไหลเวียนโลหิตป้องกันการเป็นตะคริว " +
                "โดยมือประสานกันไว้ที่หน้าท้อง ดันมือลง กระดกปลายเท้าขึ้นและเกร็งขาทั้ง 2 ข้าง ทำซ้ำเช่นนี้ <br>3 - 5 ครั้ง"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
