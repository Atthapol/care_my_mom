package com.mymom.attha.caremymom

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_page_food1.*

class PageFood1Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_food1)
        btn_back.setOnClickListener {
            val Intent = Intent(this,MenuFoodActivity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,MenuFoodActivity::class.java)
        startActivity(intent)
    }
}