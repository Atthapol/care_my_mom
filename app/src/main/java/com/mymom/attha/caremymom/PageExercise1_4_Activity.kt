package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_4.*

class PageExercise1_4_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_4)
        var textSring = ""+getString(R.string.tab)+"นั่งขัดสมาธิราบหลังเหยียดตรงหายใจเข้ากางมือทั้ง 2 ข้างระดับไหล่ หายใจออกวางศอกขวาลงด้านข้างลำตัว แขนซ้ายยกเหนือศีรษะดันมาทางขวาเอียงลำตัวให้มากที่สุด พักไว้สักครู่ หายใจเข้าออกปกติแล้วหายใจเข้ากลับมาท่าเดิมทำสลับกัน ซ้าย - ขวา <br><br>"+getString(R.string.tab)+"<b>ประโยชน์</b> บริหารกล้ามเนื้อข้างลำตัว ลดไขมันที่เอวและต้นแขน แก้ปัญหาปวดสะโพก และต้นแขน"
        val text = findViewById<TextView>(R.id.textdetail) as  TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
