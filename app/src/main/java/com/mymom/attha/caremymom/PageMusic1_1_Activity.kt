package com.mymom.attha.caremymom

import android.content.Intent
import android.content.Context
import android.os.Handler
import android.widget.SeekBar
import android.widget.Toast
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_music1_1.*

class PageMusic1_1_Activity : AppCompatActivity() {
    internal lateinit var  textview: TextView
    internal var mediaplayer: MediaPlayer? = null
    private lateinit var runnable:Runnable
    private var handler: Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_music1_1)

        textview = findViewById(R.id.text_header) as TextView
        val intent:Intent = intent
        var text = intent.getStringExtra("TEXT_HEADER")
        var music = intent.getStringExtra("TEXT_MUSIC")
        val singh = resources.getIdentifier(music,"raw",packageName)
        textview.setText(text)
//        if(mediaplayer != null){
//            mediaplayer!!.stop()
//            mediaplayer == null
//        }else{
            mediaplayer = MediaPlayer.create(this,singh)
            mediaplayer!!.start()
            toast("play")
            initializeSeekBar()
//        }
        if(mediaplayer != null){
            btn_play.imageAlpha = 75
            btn_play.isEnabled = false
            btn_pause.imageAlpha = 225
            btn_pause.isEnabled = true
        }
        mediaplayer!!.setOnCompletionListener {
            btn_play.imageAlpha = 225
            btn_play.isEnabled = true
            btn_pause.imageAlpha = 75
            btn_pause.isEnabled = false
            toast("end")
        }
        btn_back.setOnClickListener {
            if(mediaplayer != null){
                mediaplayer!!.stop()
                mediaplayer == null
            }
            val intent = Intent(this,Menu_Sub_PageMusic1_Activity::class.java)
            startActivity(intent)
        }
        btn_pause.setOnClickListener {
            mediaplayer!!.pause()
            btn_play.imageAlpha = 225
            btn_play.isEnabled = true
            btn_pause.imageAlpha = 75
            btn_pause.isEnabled = false
        }
        btn_play.setOnClickListener {
            mediaplayer!!.start()
            btn_play.imageAlpha = 75
            btn_play.isEnabled = false
            btn_pause.imageAlpha = 225
            btn_pause.isEnabled = true
        }
        btn_menu.setOnClickListener {
            if (mediaplayer != null) {
                mediaplayer!!.stop()
                mediaplayer == null
            }
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (b) {
                    mediaplayer!!.seekTo(i * 1000)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
    }

    override fun onBackPressed() {
        if(mediaplayer != null){
            mediaplayer!!.stop()
            mediaplayer == null
        }
            val intent = Intent(this,Menu_Sub_PageMusic1_Activity::class.java)
            startActivity(intent)
    }

    private fun initializeSeekBar() {
        seekBar.max = mediaplayer!!.seconds

        runnable = Runnable {
            seekBar.progress = mediaplayer!!.currentSeconds

            text_time1.text = "${ mediaplayer!!.currentSeconds} sec"
            val diff = mediaplayer!!.seconds -  mediaplayer!!.currentSeconds
            text_time2.text = "$diff sec"

            handler.postDelayed(runnable, 1000)
        }
        handler.postDelayed(runnable, 1000)
    }
    val MediaPlayer.seconds:Int
        get() {
            return this.duration / 1000
        }
    val MediaPlayer.currentSeconds:Int
        get() {
            return this.currentPosition/1000
        }
    fun Context.toast(message:String){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}

