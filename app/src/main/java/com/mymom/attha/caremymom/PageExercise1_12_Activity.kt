package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_12.*

class PageExercise1_12_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_12)
        var textSring = getString(R.string.tab)+"หายใจเข้ายกแขนข้างใดข้างหนึ่งขึ้น หายใจออกหักข้อศอกใช้มือแตะไหล่มืออีกข้างดันลง(ทำสลับกันซ้าย-ขวา)<br><br>"+getString(R.string.tab)+"<b>ประโยชน์</b> แก้ไขไหล่ติด ช่วยยืดกล้ามเนื้อข้างลำตัว"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
