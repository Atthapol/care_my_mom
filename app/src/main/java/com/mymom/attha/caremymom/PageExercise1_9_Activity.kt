package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_9.*

class PageExercise1_9_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_9)
        var textSring = getString(R.string.tab)+"นั่งลำตัวตรงฝ่าเท้าประกบกันไว้ เอามือจับเข่าทั้งสองกดลงแนบกับพื้นเท่าที่ทำได้แล้วปล่อยทำหลายๆ ครั้ง ท่านี้ช่วยให้ข้อสะโพกและกระดูกหัวเหน่าขยายออก" +
                " ช่วยให้การคลอดง่ายขึ้น ต่อมามือทั้งสองจับปลายเท้าไว้ กดเข่าชิดพื้นให้มากที่สุดหายใจเข้าช้า ๆ ยืดอกขึ้นเต็มที่ ต่อมาหายใจออกค่อยๆ ก้มตัวลงหน้าผากชิดปลายเท้าเท่าที่ทำได้ค้างสักครู่ " +
                "หายใจเข้าเงยหน้าขึ้นทำซ้ำ 2 - 3 ครั้ง หลังจากนั้นให้ฝึกขมิบก้นต่อ โดยนั่งในท่าเดิมหายใจเข้าช้า ๆ แล้วหายใจออกจนลมหมด ขมิบก้นและช่องคลอดค้างไว้ 5 - 10 วินาที" +
                " และผ่อนคลายทำซ้ำ 10 - 20 ครั้ง ฝึกเช้าเย็นทุกวันจะช่วยให้กล้ามเนื้อหูรูดมีความยืดหยุ่นขยายตัวดี<br><br>"+getString(R.string.tab)+"<b>ประโยชน์</b> ช่วยในระบบขับถ่ายปัสสาวะและลำไส้เล็ก อุ้งเชิงกราน " +
                "มดลูกและรังไข่ กระตุ้นระบบประสาทที่ควบคุมการบีบตัวของกระเพาะปัสสาวะบรรเทาอาการกลั้นปัสสาวะไม่อยู่"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
