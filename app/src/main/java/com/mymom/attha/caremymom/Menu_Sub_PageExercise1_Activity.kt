package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu__sub__page_exercise1.*

class Menu_Sub_PageExercise1_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu__sub__page_exercise1)
        btn_ex1_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_1_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex2_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_2_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex3_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_3_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex4_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_4_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex5_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_5_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex6_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_6_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex7_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_7_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex8_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_8_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex9_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_9_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex10_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_10_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex11_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_11_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex12_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_12_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex13_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_13_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex14_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_14_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex15_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_15_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex16_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_16_Activity ::class.java)
            startActivity(intent)
        }
        btn_ex17_mn.setOnClickListener{
            val intent = Intent(this,PageExercise1_17_Activity ::class.java)
            startActivity(intent)
        }
        btn_back_menu_sub_page1.setOnClickListener {
            val Intent = Intent(this,MenuPageExercise_Activity::class.java)
            startActivity(Intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,MenuPageExercise_Activity::class.java)
        startActivity(intent)
    }
}
