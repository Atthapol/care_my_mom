package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu__sub__page_music2.*

class Menu_Sub_PageMusic2_Activity : AppCompatActivity() {

    override fun onBackPressed() {
        val Intent = Intent(this,MenuPageMusic_Activity::class.java)
        startActivity(Intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu__sub__page_music2)
        btn_back_menu_sub_music2.setOnClickListener {
            val intent = Intent(this,MenuPageMusic_Activity::class.java)
            startActivity(intent)
        }
        btn_music1_mn.setOnClickListener {
            val music_header:String = "Goodnight My Angel - Billy Joel"
            val music_name:String = "goodnight_my_angel_billy_joel"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music2_mn.setOnClickListener {
            val music_header:String = "Baby Mine - Allison Krauss"
            val music_name:String = "baby_mine_allison_krauss"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music3_mn.setOnClickListener {
            val music_header:String = "Three Little Birds - Bob Marley"
            val music_name:String = "three_little_birds_bob_marley"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music4_mn.setOnClickListener {
            val music_header:String = "Better Together - Jack Johnson"
            val music_name:String = "better_together_jack_johnson"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music5_mn.setOnClickListener {
            val music_header:String = "What a Wonderful World - Louis Armstrong"
            val music_name:String = "what_a_wonderful_world_louis_armstrong"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music6_mn.setOnClickListener {
            val music_header:String = "They Long to Be Close to You - the Carpenters"
            val music_name:String = "close_to_you_the_carpenters"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music7_mn.setOnClickListener {
            val music_header:String = "Your Song - Ellie Goulding"
            val music_name:String = "your_song_ellie_goulding"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music8_mn.setOnClickListener {
            val music_header:String = "Twinkle Twinkle Little Star lyrics"
            val music_name:String = "twinkle_twinkle_little_star_lyrics"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music9_mn.setOnClickListener {
            val music_header:String = "Five Little Ducks"
            val music_name:String = "five_little_ducks"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music10_mn.setOnClickListener {
            val music_header:String = "Wheels On The Bus"
            val music_name:String = "wheels_on_the_bus"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music11_mn.setOnClickListener {
            val music_header:String = "Rudolph the Red Nosed Reindeer"
            val music_name:String = "rudolph_the_red_nosed_reindeer"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music12_mn.setOnClickListener {
            val music_header:String = "Old MacDonald Had A Farm"
            val music_name:String = "old_macdonald_had_a_farm"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music13_mn.setOnClickListener {
            val music_header:String = "Do a deer a female deer"
            val music_name:String = "do_a_deer_a_female_deer"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music14_mn.setOnClickListener {
            val music_header:String = "Are you sleeping"
            val music_name:String = "are_you_sleeping"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music15_mn.setOnClickListener {
            val music_header:String = "One Little Finger"
            val music_name:String = "one_little_finger"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_music16_mn.setOnClickListener {
            val music_header:String = "Classical Music - Mozart"
            val music_name:String = "classical_music_mozart"
            val intent = Intent(this, PageMusic2_1_Activity::class.java)
            intent.putExtra("TEXT_HEADER",music_header)
            intent.putExtra("TEXT_MUSIC",music_name)
            startActivity(intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }
}
