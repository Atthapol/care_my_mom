package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_7.*

class PageExercise1_7_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_7)
        var textSring = getString(R.string.tab)+" 1) นอนหงายราบกับพื้น กางแขนทั้งสองออกระดับไหล่ ฝ่ามือคว่ำ ยกขาขวาตั้งขึ้นให้ฝ่าเท้าวางบนเข่าซ้าย หายใจเข้าก่อน" +
                " หายใจออกเข่าขวาวางลงแนบพื้นด้านซ้ายใบหน้าหันมาทางด้านขวาค้างไว้ในท่านี้หายใจเข้าและออกปกติ<br><br>"+getString(R.string.tab)+"2) ต่อมาพลิกตัวกลับมาท่าเดิม ตั้งเข่าซ้ายขึ้น ฝ่าเท้าวางลงบนเข่าขวาหายใจเข้า" +
                " ต่อมาหายใจออกวางเข่าซ้ายลงบนพื้นด้านขวาใบหน้าหันไปทางซ้ายหายใจเข้าออกปกติ ค้างไว้สักครู่<br><br>"+getString(R.string.tab)+"3) ทำสลับข้างกันไปมา " +
                "หรือจะตั้งเข่าทั้งสองข้างก็ได้<br><br>"+getString(R.string.tab)+" <b>ประโยชน์</b> ท่านี้ช่วยบำบัดอาการท้องผูก ช่วยขับลมและทำให้ระบบการย่อยทำงานดีแก้ปวดหลัง ปวดเอว"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
