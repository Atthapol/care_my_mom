package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_16.*

class PageExercise1_16_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_16)
        var textSring = getString(R.string.tab)+"นั่งยอง ๆ เท้าห่างกันประมาณ 30 - 45 เซนติเมตร ค่อยๆ งอเข่า ย่อตัวลงนั่งให้ฝ่าเท้าแนบไปกับพื้น เข่าแยกกันโดยใช้ศอกยัน<br><br>"+getString(R.string.tab)+" <b>ประโยชน์</b> ช่วยให้กล้ามเนื้อหูรูดและกล้ามเนื้ออุ้งเชิงกรานแข็งแรง"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
