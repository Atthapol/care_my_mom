package com.mymom.attha.caremymom

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu_food.*



class MenuFoodActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_food)

        btn_menu_taimas1.setOnClickListener{
            val intent = Intent(this,PageFood1Activity::class.java)
            startActivity(intent)
        }
        btn_menu_taimas2.setOnClickListener{
            val intent = Intent(this,PageFood2Activity::class.java)
            startActivity(intent)
        }
        btn_menu_taimas3.setOnClickListener{
            val intent = Intent(this,PageFood3Activity::class.java)
            startActivity(intent)
        }
        btn_menu_avoid.setOnClickListener{
            val intent = Intent(this,PageFoodAvoidActivity::class.java)
            startActivity(intent)
        }
        btn_back.setOnClickListener {
            val intent = Intent(this, MenuPageActivity::class.java)
            startActivity(intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,MenuPageActivity::class.java)
        startActivity(intent)
    }
}