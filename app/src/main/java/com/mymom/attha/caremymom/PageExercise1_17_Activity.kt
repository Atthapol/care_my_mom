package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_page_exercise1_7.*

class PageExercise1_17_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_17)
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
