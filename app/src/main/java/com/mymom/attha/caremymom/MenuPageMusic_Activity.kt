package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu_page_music.*

class MenuPageMusic_Activity : AppCompatActivity() {

    override fun onBackPressed() {
        val Intent = Intent(this,MenuPageActivity::class.java)
        startActivity(Intent)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_page_music)
        btn_back_menu_music.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
        btn_music_thia_mode.setOnClickListener {
            val intent = Intent(this,Menu_Sub_PageMusic1_Activity::class.java)
            startActivity(intent)
        }
        btn_music_eng_mode.setOnClickListener {
            val intent = Intent(this,Menu_Sub_PageMusic2_Activity::class.java)
            startActivity(intent)
        }
        btn_menu.setOnClickListener {
            val intent = Intent(this,MenuPageActivity::class.java)
            startActivity(intent)
        }
    }
}
