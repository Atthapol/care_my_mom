package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_6.*

class PageExercise1_5_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_5)
        var textSring = getString(R.string.tab)+"ท่านี้ใช้เป็นท่าพัก ท่านี้ร่างกายจะนอนแน่นิ่งคล้ายศพจึงเรียกชื่อเช่นนี้ บางท่านอาจจะเรียกว่า ท่าผ่อนคลายก็ได้ <br><br> "+getString(R.string.tab)+" <b>การปฏิบัติ</b> นอนหงาย ทิ้งน้ำหนักตัวลงกับพื้น ขาทั้งสองข้างแยกออกจากกันเล็กน้อย แขนทั้งสองวางไว้ห่างจากลำตัวเล็กน้อย ทิ้งน้ำหนักตัวลงกับพื้นหายใจเข้าออกช้า ๆ"+
                " ทำสติดูอาการเคลื่อนไหวของหน้าท้องหรือหน้าอกตลอดเวลา ซึ่งจะทำให้เกิดความผ่อนคลายได้มาก<br><br>"+getString(R.string.tab)+" <b>ประโยชน์</b> ท่านี้ช่วยให้ร่างกายผ่อนคลายได้มาก ใช้คั่นในเวลาที่เราทำอาสนะในแต่ละชุดและเมื่อสิ้นสุดการทำอาสนะทั้งหมดเราจะพักยาวใช้เวลา 1 ใน 4 ของเวลาที่เราใช้ฝึก เช่น ฝึกมา 1 ชั่วโมง ก็ปฏิบัติท่านี้ยาว 15 นาที เป็นต้น"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
