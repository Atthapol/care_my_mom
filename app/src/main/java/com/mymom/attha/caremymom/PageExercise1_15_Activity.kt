package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_15.*

class PageExercise1_15_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_15)
        var textSring = getString(R.string.tab)+"ยืนบนเข่าหลังเท้าแนบกับพื้นเข่าชิดกัน หายใจเข้า ยกแขนข้าง ซ้ายขึ้นเหนือศีรษะ หายใจออก เอนตัวไปด้านหลัง " +
                "มือทั้งสองแตะหลังไว้ ทิ้งน้ำ หนักไปทางด้านหลัง ค้างอยู่ในท่านี้ 10 วินาที หายใจเข้าและออกในช่วงที่ค้างทำซ้ำ 2 - 3 ครั้ง<br><br>"+getString(R.string.tab)+"<b>ประโยชน์</b> ท่านี้ช่วยให้กระดูกสันหลังยืดหยุ่นดี แก้อาการไหล่ห่อและหลัง โก่ง ช่วยให้ข้อเข่าแข็งแรง"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
