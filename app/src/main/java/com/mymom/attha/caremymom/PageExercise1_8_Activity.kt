package com.mymom.attha.caremymom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_page_exercise1_8.*

class PageExercise1_8_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_exercise1_8)
        var textSring = getString(R.string.tab)+"นอนหงายราบกับพื้นชันเข่าขึ้น ยกเอวและสะโพกขึ้น เอามือยันเอวไว้แอ่นลำตัวไว้ ค้างอยู่ในท่านี้ หายใจเข้าและออกปกติ" +
                " ค้างอยู่ในท่านี้ 10 วินาที<br><br>"+getString(R.string.tab)+" <b>ประโยชน์</b> ท่านี้มีการยืดกระดูกสันหลังมาก กระดูกสันหลังจะมีความยืดหยุ่นดี ช่วยแกอาการปวดหลัง ช่วยให้แขน ขา ข้อมือ ข้อเท้าแข็งแรง " +
                "  ท่านี้ทำให้เลือดไหลเวียนในสมองได้ดี ระบบประสาทดีขึ้น แก้อาการปวดศีรษะ แก้เครียด ท่านี้ช่วยให้ระบบ การย่อยทำงานได้ดี"
        val text = findViewById<TextView>(R.id.textdetail) as TextView
        text.setText(Html.fromHtml(textSring));
        btn_back.setOnClickListener {
            val Intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
            startActivity(Intent)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this,Menu_Sub_PageExercise1_Activity::class.java)
        startActivity(intent)
    }
}
